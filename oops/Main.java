package oops;

import oops.railway.object.*;

import oops.railway.networks.*;

import oops.railway.ui.*;

import oops.io.port.*;

import oops.io.task.*;

public final class Main {

    public static final DataTranslator<RailwayNetwork> translator = new Serializer<RailwayNetwork>();

    public static final RailwayNetwork railwayNetwork = RailwaySystem.getNewOrExistingNetwork("Indian Railway Network", Main.translator, IndianRailwayNetwork.class);

    public static final RailwayUI railwayUI = new RailwayCLI(Main.railwayNetwork, Main.translator);

    public static final void main(String... args) {

            Main.railwayUI.startUI();

//             RailwaySystem.deleteNetwork(Main.railwayNetwork);

            FileManipulator.deleteClassFiles("C:/Users/user/Desktop/OOPS/", "", "lang", "lang/reflect", "util", "io/port", "io/task", "graph/lang", "graph/util", "railway/", "railway/networks", "railway/object", "railway/validator", "railway/ui");

    }

}

/*

USERS:

RailwayUser.Info[userName=NavinSK, aadharNumber=412518105042, eMailID=sknavin369@gmail.com, password=Me_369@Jesus


**********************
NavinSK
412518105042
sknavin369@gmail.com
Me_369@Jesus
**********************


RailwayUser.Info[userName=Shivam7936, aadharNumber=412518105043, eMailID=shivamsk7936@yahoo.com, password=You3@Eesan

*/