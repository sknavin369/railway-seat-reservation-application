package oops.util;

import java.util.*;

public final class StringUtil {

    private StringUtil() {}

    public static String getStringWithSuffixForCount(String string, int count) {

        return Integer.toString(count) + " " + string + (count == 1 ? "" : "s");

    }

    public static String repeat(String str, int times) {

        if(str == null)

            return "";

        return new String(new char[times]).replace("\0", str);

    }

    public static String repeat(Character character, int times) {

        if(character == null)

            return "";

        return StringUtil.repeat(Character.toString(character), times);

    }

    public static String getBulletedText(String bullet, String separator, String[] args0, String... args1) {

        if(args0 == null && args1 == null)

            return "";

        String ans = "";

        for(String arg: ArraysUtil.mergeToList(args0, args1))

            ans += bullet + " " + arg + separator;

        return ans;

    }

    public static String getItemsInOrder(String separator, Object... items) {

        String text = "";

        for(int index = 0; index < items.length;)

            text += Integer.toString(++index) + ". " + items[index-1].toString() + (index != items.length && separator != null ? separator : "");

        return text;

    }

    public static String arrayToString(String separator, Object... array) {

        String ans = "";

        for(int i = 0; i < array.length; i++)

            ans += array[i].toString() + (i == array.length - 1 || separator == null ? "" : separator);

        return ans;

    }

}