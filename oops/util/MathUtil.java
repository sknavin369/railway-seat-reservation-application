package oops.util;

import java.util.*;

public final class MathUtil {

    private MathUtil() {}

    public static String getNumeralWithOrdinalIndicator(int value) {

        value = Math.abs(value);

        return Integer.toString(value) + ((value / 10) % 10 == 1 ? "th" : value % 10 == 1 ? "st" : value % 10 == 2 ? "nd" : value % 10 == 3 ? "rd" : "th");

    }

}