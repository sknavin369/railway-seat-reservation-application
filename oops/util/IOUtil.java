package oops.util;

import java.util.*;

import java.io.*;

public final class IOUtil {

    private IOUtil() {}

    public static boolean deleteFile(String fileName) {

        return new File(fileName).delete();

    }

    public static void deleteDir(String dirName) {

        File dirFile = new File(dirName);

        if(dirFile.isDirectory())

            Arrays.stream(dirFile.listFiles()).forEach(file -> IOUtil.deleteDir(file.getPath()));

        IOUtil.deleteFile(dirName);

    }

    public static void deleteFileAndDirIfEmpty(String fileName, String filePath) {

        File file = new File(filePath + "/" + fileName), dir = new File(filePath);

        file.delete();

        if(dir.list().length == 0)

            dir.delete();

    }

    public static void mkdirs(String pathName) {

        new File(pathName).mkdirs();

    }

    private static void addTextTo(String pathName, String text, boolean isAppend) throws FileNotFoundException, IOException {

        try(PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(pathName, isAppend)))) {

            printWriter.println(text);

        }

        catch(Exception exception) {

            throw exception;

        }

    }

    public static void writeTextTo(String pathName, String text) throws FileNotFoundException, IOException {

        IOUtil.addTextTo(pathName, text, false);

    }

    public static void appendTextTo(String pathName, String text) throws FileNotFoundException, IOException {

        IOUtil.addTextTo(pathName, text, true);

    }

}