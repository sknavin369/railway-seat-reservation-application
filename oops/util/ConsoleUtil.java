package oops.util;

import oops.lang.*;

import oops.io.task.*;

import java.util.*;

import java.io.*;

import java.util.regex.*;

public final class ConsoleUtil {

    public static final Scanner scanner = new Scanner(System.in);

    public static final Console console = System.console();

    public static final ProcessBuilder processBuilderForWindows = new ProcessBuilder("cmd", "/c", "cls").inheritIO();

    private ConsoleUtil() {}

    public static void repeat(Character character, int times) {

        System.out.println(StringUtil.repeat(character, times));

    }

    public static String readPassword() {

// One may, modify this method to scanner.nextLine() method, if they want to view the password in console
        return new String(ConsoleUtil.console.readPassword());

    }

    public static String nextLine() {

        return ConsoleUtil.scanner.nextLine();

    }

    public static void flushConsole() {

// Classic way of cleaning, console by repeating '\n' for some times in console
//         ConsoleUtil.repeat('\n', 70);

        try {

            if(System.getProperty("os.name").contains("Windows"))

                ConsoleUtil.processBuilderForWindows.start().waitFor();

            else

                Runtime.getRuntime().exec("clear");

        }

        catch(InterruptedException | IOException exception) {

            exception.printStackTrace();

        }

    }

    public static void loadTillCompletion(Thread thread) {

        System.out.print("\nLoading...");

        thread.start();

        try {

            thread.join();

        }

        catch(InterruptedException exception) {

            exception.printStackTrace();

        }

        ConsoleUtil.flushConsole();

    }

    public static void waitForPress() {

        System.out.print("\n\nPress Enter to continue... ");

        ConsoleUtil.console.readPassword();

        ConsoleUtil.flushConsole();

    }

    public static void waitForPress(String message) {

        System.out.println(message);

        ConsoleUtil.waitForPress();

    }

    public static void loadAndFlushFor(int time) {

        System.out.print("\nLoading.");
        ThreadUtil.sleep(time / 3);

        System.out.print(".");
        ThreadUtil.sleep(time / 3);

        System.out.println(".");
        ThreadUtil.sleep(time / 3);

        ConsoleUtil.flushConsole();

    }

    public static void justFlushFor(int time) {

        ThreadUtil.sleep(time);

        ConsoleUtil.flushConsole();

    }

    public static int getChoosenOption(Character separator, String title, String delimiter, Object... choices) {

        int option;


        String text = "\t\t\t" + title + "\n\n";


        if(delimiter != null)

            delimiter = delimiter.trim();


        if(delimiter != null && delimiter.length() != 0)

            text += "NOTE:\n-> Enter '" + delimiter + "' to go back\n\n\n";


        text += StringUtil.getItemsInOrder((separator != null ? "\n\n" + StringUtil.repeat(separator, 120) : "") + "\n\n", choices) + "\n\n";


        System.out.print(text);

        while(true) {

            System.out.print("\nChoose any of these options: ");

            option = ConsoleUtil.validateOption(ConsoleUtil.scanner.nextLine().trim(), delimiter, choices.length);

            ConsoleUtil.flushConsole();

            if(option > -1)

                break;

            System.out.println(text + "\nINVALID OPTION! ENTER A VALID CHOICE.");

        }

        return option;

    }

    private static int validateOption(String currentOption, String delimiter, int limit) {

        int option;

        if(currentOption.equals(delimiter))

            return 0;

        else if(!currentOption.matches("\\d+") || ((option = Integer.parseInt(currentOption)) < 1 || option > limit))

            return -1;

        return option;

    }

    public static boolean getYesOrNoResponseFor(String message) {

        String response;

        while(true) {

            System.out.print("\n" + message + " (y/n): ");

            response = ConsoleUtil.scanner.nextLine().trim();

            ConsoleUtil.flushConsole();

            if(response.equals("y") || response.equals("n"))

                break;

            System.out.print("INVALID OPTION! ENTER A VALID CHOICE.\n");

        }

        return response.charAt(0) == 'y';

    }

    public static boolean getYesOrNoResponse(String message) {

        String response;

        System.out.print("\n" + message);

        while(true) {

            System.out.print("\n\nDo you like to accept? (y/n): ");

            response = ConsoleUtil.scanner.nextLine().trim();

            ConsoleUtil.flushConsole();

            if(response.equals("y") || response.equals("n"))

                break;

            System.out.print("\n" + message + "\n\nINVALID OPTION! ENTER A VALID CHOICE.");

        }

        return response.charAt(0) == 'y';

    }

    public static String[] getInputsFromInvokers(String title, Message<String> message, String[] notes, InputInvoker... inputInvokers) {

        boolean exceptionCaught = false;

        String ans[] = new String[inputInvokers.length], input, exceptionStringRep;

        String consoleText = "\t\t\t" + title + "\n" +
                             StringUtil.repeat('_', 69) + "\n\n" +
                             (message != null ? message.getTitle() + "\n\n" + StringUtil.getBulletedText("->", "\n", message.getMessages()) + "\n" : "") +
                             "NOTE:\n" +
                             StringUtil.getBulletedText("->", "\n", notes, "Enter '-1' to go back!") + "\n\n";

        InputInvoker inputInvoker = null;

        System.out.print(consoleText);

        for(int index = 0; index < inputInvokers.length;) {

            inputInvoker = inputInvokers[index];

            try {

                input = (String) inputInvoker.invokeInConsole("-1");

                if(input.compareTo("-1") == 0) {

                    ConsoleUtil.flushConsole();

                    return null;

                }

                ans[index++] = input;

                consoleText += inputInvoker.getQuestion() + inputInvoker.getInput() + "\n\n\n";

                System.out.print("\n\n");

                if(exceptionCaught) {

                    exceptionCaught = false;

                    ConsoleUtil.flushConsole();

                    System.out.print(consoleText);

                }

            }

            catch(Exception exception) {

                exceptionCaught = true;

                ConsoleUtil.flushConsole();

                exceptionStringRep = exception.getCause().toString();

                System.out.println(consoleText + "!!!WARNING: " + exceptionStringRep.substring(exceptionStringRep.indexOf(':') + 2) + "!!!\n");

            }

        }

        ConsoleUtil.flushConsole();

        return ans;

    }

    public static InputInvoker[] getInputsFromRepetitiveInvokers(String title, Message<String> message, String[] notes, Integer maxLimit, InputInvoker... inputInvokers) {

        boolean exceptionCaught = false;

        String input, exceptionStringRep;

        String consoleText = "\t\t\t" + title + "\n" +
                             StringUtil.repeat('_', 69) + "\n\n" +
                             (message != null ? message.getTitle() + "\n\n" + StringUtil.getBulletedText("->", "\n", message.getMessages()) + "\n" : "") +
                             "NOTE:\n" +
                             StringUtil.getBulletedText("->", "\n", notes, "Enter '-1' to go back!", "Enter 'DONE' to stop!") + "\n\n";

        InputInvoker inputInvoker = null;

        System.out.print(consoleText);

        for(int i = 1, j; i <= maxLimit; i++) {

            for(j = 0; j < inputInvokers.length;) {

                inputInvoker = inputInvokers[j];

                try {

                    System.out.print("Enter " + MathUtil.getNumeralWithOrdinalIndicator(i) + " ");

                    input = (String) inputInvoker.invokeInConsole("-1", "DONE");

                    if(input.compareTo("-1") == 0) {

                        ConsoleUtil.flushConsole();

                        return null;

                    }

                    else if(input.compareTo("DONE") == 0) {

                        ConsoleUtil.flushConsole();

                        return inputInvokers;

                    }

                    consoleText += "Enter " + MathUtil.getNumeralWithOrdinalIndicator(i) + " " + inputInvoker.getQuestion() + inputInvoker.getInput() + "\n\n\n";

                    System.out.print("\n\n");

                    j++;

                    if(exceptionCaught) {

                        exceptionCaught = false;

                        ConsoleUtil.flushConsole();

                        System.out.print(consoleText);

                    }

                }

                catch(Exception exception) {

                    exceptionCaught = true;

                    ConsoleUtil.flushConsole();

                    exceptionStringRep = exception.getCause().toString();

                    System.out.println(consoleText + "!!!WARNING: " + exceptionStringRep.substring(exceptionStringRep.indexOf(':') + 2) + "!!!\n");

                }

            }

        }

        ConsoleUtil.flushConsole();

        return inputInvokers;

    }

}