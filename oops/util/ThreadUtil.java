package oops.util;

import java.io.*;

public final class ThreadUtil {

    private ThreadUtil() {}

    public static void sleep(int time) {

        try {

            Thread.sleep(time);

        }

        catch(InterruptedException exception) {

            exception.printStackTrace();

        }

    }

}