package oops.io.task;

import oops.util.*;

import oops.lang.reflect.*;


import java.security.*;

import java.lang.reflect.*;

public class InputInvoker {

    private final String question;

    private final MethodInvoker inputReader;

    private final MethodInvoker inputValidator;

    private String input = null;

    public InputInvoker(String question, MethodInvoker inputReader, MethodInvoker inputValidator) {

        this.question = question;

        this.inputReader = inputReader;

        this.inputValidator = inputValidator;

    }

    public String getQuestion() {

        return this.question;

    }

    public MethodInvoker getInputReader() {

        return this.inputReader;

    }

    public MethodInvoker getInputValidator() {

        return this.inputValidator;

    }

    public String getInput() {

        return this.input;

    }

    public Object invoke(String... delimiters) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        Object input = this.getInputReader().evalMethod();

        this.input = (String) input;


        if(~ArraysUtil.indexOf(delimiters, this.input) != 0)

            return this.input;


        this.getInputValidator().addArgument(input);

        try {

            this.getInputValidator().evalMethod();

        }

        catch(Exception exception) {

            this.getInputValidator().removeArgument(input);

            throw exception;

        }

        this.getInputValidator().removeArgument(input);

        return input;

    }

    public Object invokeInConsole(String... delimiters) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        System.out.print(this.getQuestion());

        return this.invoke(delimiters);

    }

}