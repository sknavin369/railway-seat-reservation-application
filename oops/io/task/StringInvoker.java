package oops.io.task;

import oops.lang.reflect.*;

import oops.util.*;

public class StringInvoker extends InputInvoker {

    public StringInvoker(String question, MethodInvoker inputValidator) {

        super(question, new MethodInvoker(null, ConsoleUtil.class, "nextLine"), inputValidator);

    }

}