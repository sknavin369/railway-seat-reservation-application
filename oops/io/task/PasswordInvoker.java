package oops.io.task;

import oops.lang.reflect.*;

import oops.util.*;

public class PasswordInvoker extends InputInvoker {

    public PasswordInvoker(String question, MethodInvoker inputValidator) {

        super(question, new MethodInvoker(null, ConsoleUtil.class, "readPassword"), inputValidator);

    }

    @Override
    public final String getInput() {

        return "";

    }

}