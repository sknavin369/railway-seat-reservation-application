package oops.io.port;

import java.io.*;

public interface DataTranslator<T extends Object> extends Serializable {

    void save(T obj, File file);

    T retrieve(File file);

}