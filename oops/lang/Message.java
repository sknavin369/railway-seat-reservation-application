package oops.lang;

public class Message<T> {

    private final String title;

    private final T[] messages;

    public Message(String title, T[] messages) {

        this.title = title;

        this.messages = messages;

    }

    public String getTitle() {

        return this.title;

    }

    public T[] getMessages() {

        return this.messages;

    }

}