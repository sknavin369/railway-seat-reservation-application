package oops.lang.reflect;

import java.security.*;

import java.lang.reflect.*;

import java.util.*;

public final class MethodInvoker {

    private final Object object;

    private final Class<?> classOfObject;

    private final String methodName;

    private final List<Object> args = new ArrayList<Object>();

    private int argSize;


    public MethodInvoker() {

        this(null, null);

    }

    public MethodInvoker(Class<?> classOfObject, String methodName, Object... args) throws InvalidParameterException {

        this(null, classOfObject, methodName, args);

    }

    public MethodInvoker(Object object, Class<?> classOfObject, String methodName, Object... args) throws InvalidParameterException {

        if(object != null && object.getClass() != classOfObject)

            throw new InvalidParameterException("The run-time class of the object is not same as the 2nd parameter.");


        this.object = object;

        this.classOfObject = classOfObject;

        this.methodName = methodName;

        if(args != null)

            for(Object arg: args)

                this.args.add(arg);

        this.argSize = args.length;

    }

    public Object getObject() {

        return this.object;

    }

    public void addArgument(Object arg) {

        this.args.add(arg);

        this.argSize++;

    }

    public void removeArgument(Object arg) {

        this.args.remove(arg);

        this.argSize--;

    }

    public Object evalMethod() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        if(classOfObject == null)

            return null;


        final Class[] classOfArgs = this.args.stream().map(e -> e.getClass()).toArray(Class[]::new);

        return classOfObject.getMethod(this.methodName, classOfArgs).invoke(this.getObject(), args.toArray());

    }

}