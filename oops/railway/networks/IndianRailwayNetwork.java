package oops.railway.networks;

import oops.railway.object.*;

import oops.io.port.*;

import oops.lang.*;

import java.util.*;

public class IndianRailwayNetwork extends RailwayNetwork {

    private static final long serialVersionUID = 5422241985683875261L;

    private static final String welcomeMessage = "\nHon'ble Prime Minister message on \"Public Health Response to COVID-19: Campaign for COVID-Appropriate Behaviour\"." +
                                                 "\n-> Wear Mask, Follow Physical Distancing, Maintain Hand Hygine." +
                                                 "\n-> Get your favourite food at your train seat through e-Catering available at selected stations." +
                                                 "\n\n\t\t\t\tCOVID 19 ALERT" +
                                                 "\n\nInformation on Covid 19 Vaccination Programme:" +
                                                 "\n-> Covid-19 Guidelines For Kumbh Mela 2021 at Haridwar." +
                                                 "\n-> All passengers are hereby informed that downloading of Aarogya Setu App on their mobile phone, that they are carrying along, is advisable." +
                                                 "\n-> All Passenger to kindly note that on arrival at their destination, the traveling passengers will have to adhere to such health protocols as are prescribed by the destination State/UT." +
                                                 "\n-> Though various State Governments' Advisories have been provided on IRCTC Website, still Users are advised to surf Destination State Government URL/ Website for latest instructions on Covid-19 pandemic and Covid appropriate behaviour." +
                                                 "\n-> Catering Service is not available and catering charges not included in the fare." +
                                                 "\n-> No blanket and linen shall be provided in the train.";


    private static final String[] stationNames = new String[] {"Madhura", "Ayodhya", "Varanasi", "Kolkata", "Bangalore",
                                                               "Chennai", "Coimbatore", "Pune", "Mumbai", "Delhi"};

    private static final Map<String, String[]> trainDetails = new HashMap<String, String[]>() {{
        this.put("Chennai Express",        new String[] {stationNames[5], stationNames[4], stationNames[2], stationNames[1], stationNames[0]});
        this.put("P2B Express",            new String[] {stationNames[7], stationNames[8], stationNames[3], stationNames[5], stationNames[6], stationNames[4]});
        this.put("Madhura Express",        new String[] {stationNames[0], stationNames[2], stationNames[1], stationNames[8], stationNames[5], stationNames[6]});
        this.put("C2M Super-Fast Express", new String[] {stationNames[5], stationNames[3], stationNames[1], stationNames[0]});
        this.put("North Express",          new String[] {stationNames[0], stationNames[4], stationNames[2], stationNames[1], stationNames[7]});
        this.put("East Express",           new String[] {stationNames[7], stationNames[6], stationNames[8], stationNames[9], stationNames[3]});}};


    public IndianRailwayNetwork(String networkName, DataTranslator<RailwayNetwork> translator) {

        super(networkName, translator, IndianRailwayNetwork.stationNames, IndianRailwayNetwork.trainDetails);

    }

    @Override
    public String getWelcomeMessage() {

        return this.welcomeMessage;

    }

    @Override
    public String getUIString() {

        return "Aadhar";

    }

    @Override
    public void validateUINumber(String aadharNumber) throws IllegalArgumentException {

        if(!aadharNumber.matches("\\d+"))

            throw new IllegalArgumentException("INVALID AADHAR NUMBER! AADHAR NUMBER MUST CONTAIN ONLY DIGITS!");

        else if(aadharNumber.length() != 12)

            throw new IllegalArgumentException("INVALID AADHAR NUMBER! AADHAR NUMBER MUST CONTAIN 12 DIGITS!");

    }

}