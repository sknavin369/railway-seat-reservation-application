package oops.railway.validator;

import oops.railway.object.*;

import java.util.*;

import java.util.regex.*;

import java.security.*;

public final class SeatCancellationValidator {

    private final String uiNumberString;

    private final RailwayNetwork railwayNetwork;

    private final RailwayTicket railwayTicket;

    private final Set<RailwayPassenger> railwayPassengers = new HashSet<RailwayPassenger>();

    public SeatCancellationValidator(RailwayNetwork railwayNetwork, RailwayTicket railwayTicket) {

        this.railwayNetwork = railwayNetwork;

        this.railwayTicket = railwayTicket;

        this.uiNumberString = railwayNetwork.getUIString();

    }

    public void validateUINumber(String uiNumberStringRep) throws IllegalArgumentException, InvalidParameterException {

        RailwayPassenger railwayPassenger = null;

        this.railwayNetwork.validateUINumber(uiNumberStringRep);

        if((railwayPassenger = this.railwayTicket.getPassengerByUINumber(Long.parseLong(uiNumberStringRep))) == null)

            throw new InvalidParameterException("INVALID " + this.uiNumberString + " NUMBER! THE " + this.uiNumberString + " NUMBER DOESN'T MATCH TO THAT OF THE PASSENGER'S IN THIS TICKET!");

        else if(this.railwayPassengers.contains(railwayPassenger))

            throw new InvalidParameterException("INVALID " + this.uiNumberString + " NUMBER! THIS " + this.uiNumberString + " HAS BEEN ENTERED ALREADY!");

        this.railwayPassengers.add(railwayPassenger);

    }

    public Set<RailwayPassenger> getPassengers() {

        return this.railwayPassengers;

    }

}