package oops.railway.validator;

import oops.railway.object.*;

import oops.railway.networks.*;

import java.util.*;

import java.util.regex.*;

public final class UserDetailUpdateValidator {

    private final RegistrationValidator registrationValidator;

    private final RailwayNetwork railwayNetwork;

    private final RailwayUser railwayUser;

    public UserDetailUpdateValidator(RailwayNetwork railwayNetwork, RailwayUser railwayUser) {

        this.railwayNetwork = railwayNetwork;

        this.railwayUser = railwayUser;

        this.registrationValidator = new RegistrationValidator(railwayNetwork);

    }

    public void validateNewUserName(String userName) throws IllegalArgumentException {

        if(this.railwayUser.info.userName.equals(userName))

            throw new IllegalArgumentException("THE NEW USER NAME IS SAME AS THE OLDER!");

        this.registrationValidator.validateUserName(userName);

        this.railwayUser.updateUserName(userName);

    }

    public void validateNewUINumber(String uiNumber) throws IllegalArgumentException {

        if(this.railwayUser.info.uiNumber == Long.parseLong(uiNumber))

            throw new IllegalArgumentException("THE NEW " + this.railwayNetwork.getUIString() + " NUMBER IS SAME AS THE OLDER!");

        this.registrationValidator.validateUINumber(uiNumber);

        this.railwayUser.updateUINumber(Long.parseLong(uiNumber));

    }

    public void validateNewEMailID(String eMailID) throws IllegalArgumentException {

        if(this.railwayUser.info.eMailID.equals(eMailID))

            throw new IllegalArgumentException("THE NEW E-MAIL ID IS SAME AS THE OLDER!");

        this.registrationValidator.validateEMailID(eMailID);

        this.railwayUser.updateEMailID(eMailID);

    }

    public void validateNewPassword(String password) throws IllegalArgumentException {

        if(this.railwayUser.info.verifyPassword(password))

            throw new IllegalArgumentException("THE NEW PASSWORD IS SAME AS THE OLDER!");

        this.registrationValidator.validatePassword(password);

        this.railwayUser.updatePassword(password);

    }

}