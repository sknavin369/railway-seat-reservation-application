package oops.railway.validator;

import oops.railway.object.*;

import oops.railway.networks.*;

import java.util.*;

import java.util.regex.*;

import java.security.*;

public final class LoginValidator {

    private final RailwayNetwork railwayNetwork;

    private RailwayUser railwayUser = null;


    public LoginValidator(RailwayNetwork railwayNetwork) {

        this.railwayNetwork = railwayNetwork;

    }

    /* ******************* */

    public void validateUserName(String userName) throws IllegalArgumentException {

        if(!userName.matches("\\w+"))

            throw new IllegalArgumentException("INVALID USER NAME! ENTER A VALID USER NAME!");


        this.railwayUser = this.railwayNetwork.getUserByName(userName);

        if(this.railwayUser == null)

            throw new IllegalArgumentException("INVALID USER NAME! USER NAME DOESN'T EXIST IN THIS NETWORK!");

    }

    public void validatePasswordFor(String password) throws IllegalArgumentException {

        LoginValidator.verifyPasswordFor(this.getRailwayUser(), password);

    }

    /* ******************* */

    public RailwayUser getRailwayUser() {

        return this.railwayUser;

    }

    /* ******************* */

    public static void verifyPasswordFor(RailwayUser railwayUser, String password) throws IllegalArgumentException {

        if(!RailwayUser.Info.validatePassword(password))

            throw new IllegalArgumentException("INVALID PASSWORD! ENTER A VALID PASSWORD!");

        else if(!railwayUser.info.verifyPassword(password))

            throw new IllegalArgumentException("INCORRECT PASSWORD!");

    }

}