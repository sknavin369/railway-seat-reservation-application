package oops.railway.validator;

import oops.railway.object.*;

import java.util.*;

import java.util.regex.*;

import java.security.*;

public final class ReservationValidator {

    private final Map<Long, String> passengers = new LinkedHashMap<Long, String>();

    private final RailwayNetwork railwayNetwork;

    private String lastPassengerName = null;

    /* ******************* */

    public ReservationValidator(RailwayNetwork railwayNetwork) {

        this.railwayNetwork = railwayNetwork;

    }

    public Map<Long, String> getPassengers() {

        return this.passengers;

    }

    public void validatePassengerName(String passengerName) throws IllegalArgumentException {

        if(!passengerName.matches("^[a-zA-Z ]++"))

            throw new IllegalArgumentException("INVALID PASSENGER NAME! ENTER A VALID PASSENGER NAME CONSISTING OF ALPHABETS ONLY!");

        this.lastPassengerName = passengerName;

    }

    public void validateUINumber(String uiNumberStringRep) throws IllegalArgumentException {

        this.railwayNetwork.validateUINumber(uiNumberStringRep);

        if(this.passengers.containsKey(Long.parseLong(uiNumberStringRep)))

            throw new IllegalArgumentException("INVALID " + this.railwayNetwork.getUIString() + " NUMBER! THIS " + this.railwayNetwork.getUIString() + " NUMBER HAS BEEN ENTERED ALREADY!");

        this.passengers.put(Long.parseLong(uiNumberStringRep), this.lastPassengerName);

        this.lastPassengerName = null;

    }

}