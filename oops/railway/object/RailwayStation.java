package oops.railway.object;

import oops.graph.lang.*;
import oops.graph.util.*;

import java.util.*;

public final class RailwayStation extends AdjacencyListNode<RailwayBranch> {

    private static final long serialVersionUID = 4748118272823231331L;

    public RailwayStation(String stationName) {

        super(stationName);

    }

    @Override
    public String toString() {

        return super.getName();

    }

}