package oops.railway.object;

import oops.graph.lang.*;

import oops.graph.util.*;

import java.util.*;

import java.util.stream.*;

public final class RailwayBranch extends AdjacencyListBranch<RailwayStation> {

    /* ********************** */

    private static final long serialVersionUID = 1516374757496656700L;

    static final int MAX_CONFIRMING_SEATS = 8, MAX_WAITING_SEATS = 5, MAX_BOOKING_SEATS = 6;

    private final RailwayTrain railwayTrain;

    /* ********************** */

    private final Set<Integer> ticketSet = new HashSet<Integer>(IntStream.range(1, RailwayBranch.MAX_CONFIRMING_SEATS + 1).boxed().collect(Collectors.toList()));

    private final Map<RailwayPassenger, Integer> passengerSeatMap = new TreeMap<RailwayPassenger, Integer>();

    private final Set<RailwayPassenger> waitingSet = new LinkedHashSet<RailwayPassenger>();

    /* ********************** */

    public void setSeatForPassenger(RailwayPassenger railwayPassenger, Integer seatNumber) {

        if(~seatNumber != 0) {

            this.passengerSeatMap.put(railwayPassenger, seatNumber);

            this.ticketSet.remove(seatNumber);

            this.waitingSet.remove(railwayPassenger);

        }

        else

            this.waitingSet.add(railwayPassenger);

    }

// true if removed from booked seats
    public boolean removeSeatForPassenger(RailwayPassenger railwayPassenger) {

        Integer seatNumber = this.passengerSeatMap.remove(railwayPassenger);

        if(seatNumber != null)

            this.ticketSet.add(seatNumber);

        else

            this.waitingSet.remove(railwayPassenger);

        return seatNumber != null;

    }

    /* ********************** */

    public Integer getSeatForPassenger(RailwayPassenger railwayPassenger) {

        return this.passengerSeatMap.get(railwayPassenger);

    }

    /* ********************** */

    public Set<Integer> getTicketSet() {

        return this.ticketSet;

    }

    public Map<RailwayPassenger, Integer> getPassengerSeatMap() {

        return this.passengerSeatMap;

    }

    public Set<RailwayPassenger> getWaitingSet() {

        return this.waitingSet;

    }

    public int getWaitingCount() {

        return RailwayBranch.MAX_WAITING_SEATS - this.waitingSet.size();

    }

    /* ********************** */

    /* ********************** */

    public RailwayBranch(RailwayTrain railwayTrain, Graph.TerminalPair<RailwayStation> terminal) {

        super(terminal.toString(), terminal);

        this.railwayTrain = railwayTrain;

    }

    /* ********************** */

    /* ********************** */

    public static int getTotalSeats() {

        return RailwayBranch.MAX_CONFIRMING_SEATS + RailwayBranch.MAX_WAITING_SEATS;

    }

    public static int getConfirmableTicketCountFor(int availableTicketCount) {

        return Math.max(availableTicketCount - RailwayBranch.MAX_WAITING_SEATS, 0);

    }

    public static int getWaitableTicketCountFor(int availableTicketCount) {

        return RailwayBranch.getTotalSeats() - Math.max(RailwayBranch.MAX_CONFIRMING_SEATS, RailwayBranch.getTotalSeats() - availableTicketCount);

    }

    /* ********************** */

    /* ********************** */

    public RailwayTrain getRailwayTrain() {

        return this.railwayTrain;

    }

    /* ********************** */

    /* ********************** */

    @Override
    public String toString() {

        return super.getName();

    }

}