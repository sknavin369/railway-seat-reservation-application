package oops.railway.object;

import java.io.*;

import java.util.*;

public final class RailwayPassenger implements Serializable, Comparable<RailwayPassenger> {

    private static final long serialVersionUID = 1516374757496656799L;

    /* ********************** */

    private final String name;

    private final long uiNumber;

    private RailwayTicket railwayTicket;

    public RailwayPassenger(String name, long uiNumber) {

        this.name = name;

        this.uiNumber = uiNumber;

    }

    public String getName() {

        return this.name;

    }

    public long getUINumber() {

        return this.uiNumber;

    }

    /* ********************** */

    void setRailwayTicket(RailwayTicket railwayTicket) {

        this.railwayTicket = railwayTicket;

    }

    RailwayTicket getRailwayTicket() {

        return this.railwayTicket;

    }

    public long getPNRNumber() {

        return this.railwayTicket.getPNRNumber();

    }

    /* ********************** */

    public String getPassengerDetail() {

        return "Name: " + this.name + " | " + this.getRailwayTicket().getRailwayNetwork().getUIString() + " Number: " + Long.toString(this.uiNumber);

    }

    /* ********************** */

    public static Set<RailwayPassenger> getRailwayPassengersFrom(Map<Long, String> passengers) {

        Set<RailwayPassenger> passengerSet = new LinkedHashSet<RailwayPassenger>();

        for(Map.Entry<Long, String> entry: passengers.entrySet())

            passengerSet.add(new RailwayPassenger(entry.getValue(), entry.getKey()));

        return passengerSet;

    }

    /* ********************** */

    @Override
    public int compareTo(RailwayPassenger anotherPassenger) {

        int strCompare = this.getName().compareTo(anotherPassenger.getName());

        if(strCompare != 0)

            return strCompare;

        return (int) (this.getUINumber() - anotherPassenger.getUINumber());

    }

    @Override
    public String toString() {

        return "PNR Number: " + this.getPNRNumber() + " | " + this.getPassengerDetail();

    }

}