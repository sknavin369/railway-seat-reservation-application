package oops.railway.object;

import oops.graph.lang.*;

import oops.graph.util.*;

import oops.io.port.*;

import oops.lang.*;

import oops.util.*;

import java.security.*;

import java.util.*;

import java.io.*;

public abstract class RailwayNetwork extends AdjacenyListGraph<RailwayStation> {

    private static final long serialVersionUID = -8464835276140212500L;

    final File dataFile;

    private final DataTranslator<RailwayNetwork> translator;

    private final Set<RailwayTrain> railwayTrains = new HashSet<RailwayTrain>();

    private final Map<String, RailwayUser> railwayUsers = new HashMap<String, RailwayUser>();

    private final Map<Long, RailwayTicket> railwayTickets = new HashMap<Long, RailwayTicket>();


    /* ********************* */

    protected RailwayNetwork(String networkName, DataTranslator<RailwayNetwork> translator, String[] stationNames, Map<String, String[]> trainDetails) {

        super(networkName);

        this.dataFile = new File(RailwaySystem.networkPath + super.getName());

        this.translator = translator;


        final Map<String, RailwayStation> stationNameMap = new HashMap<String, RailwayStation>();


        for(String stationName: stationNames) {

            RailwayStation railwayStation = new RailwayStation(stationName);

            stationNameMap.put(stationName, railwayStation);

            this.addStations(railwayStation);

        }

        for(Map.Entry<String, String[]> entry: trainDetails.entrySet())

            this.addNewTrains(new RailwayTrain(this, entry.getKey(), Arrays.stream(entry.getValue()).map(stationNameMap::get).toArray(RailwayStation[]::new)));


        RailwaySystem.saveExistingNetwork(this, this.getTranslator());

    }

    /* ********************* */

    abstract public String getWelcomeMessage();

    abstract public String getUIString();

    abstract public void validateUINumber(String uiNumber) throws IllegalArgumentException;

    /* ********************* */

    private void addStations(RailwayStation... railwayStations) {

        super.addNodes(railwayStations);

    }

    private void addNewTrains(RailwayTrain... railwayTrains) {

        Arrays.stream(railwayTrains).forEach(this.railwayTrains::add);

    }

    /* ********************* */

    public final RailwayUser getNewRailwayUser(String... args) throws NumberFormatException, IllegalAccessException, IllegalArgumentException, InvalidParameterException {

        if(args.length != 4)

            throw new InvalidParameterException("INVALID NUMBER OF ARGUEMENTS!");

        RailwayUser.Info info = new RailwayUser.Info(args[0], Long.parseLong(args[1]), args[2], args[3]);

        RailwayUser railwayUser = new RailwayUser(this, info);

        this.addUsers(railwayUser);

        return railwayUser;

    }

    public final RailwayTicket getNewTicket(RailwayUser railwayUser, RailwayPath railwayPath, Set<RailwayPassenger> railwayPassengers) {

        RailwayTicket railwayTicket = new RailwayTicket(railwayUser, railwayPath, railwayPassengers);

        RailwaySystem.setTicketStatusToFile(this, railwayTicket);

        return railwayTicket;

    }

    void addTicket(RailwayTicket railwayTicket) {

        this.railwayTickets.put(railwayTicket.getPNRNumber(), railwayTicket);

        RailwaySystem.saveExistingNetwork(this, this.getTranslator());

    }

    void removeTicket(RailwayTicket railwayTicket) {

        this.railwayTickets.remove(railwayTicket.getPNRNumber());

        RailwaySystem.saveExistingNetwork(this, this.getTranslator());

    }

    public RailwayTicket getTicketByPNRNumber(long pnrNumber) {

        return railwayTickets.get(pnrNumber);

    }

    void addUsers(RailwayUser... railwayUsers) {

        for(RailwayUser railwayUser: railwayUsers)

            this.railwayUsers.put(railwayUser.info.userName, railwayUser);

        RailwaySystem.saveExistingNetwork(this, this.getTranslator());

    }

    void removeUsers(RailwayUser... railwayUsers) {

        for(RailwayUser railwayUser: railwayUsers)

            this.railwayUsers.remove(railwayUser.info.userName);

    }

    public boolean searchUserByName(String userName) {

        return this.getUserByName(userName) != null;

    }

    public RailwayUser getUserByName(String userName) {

        return this.railwayUsers.get(userName);

    }

    public RailwayStation getStationByname(String name) {

        for(RailwayStation railwayStation: super.getNodes())

            if(railwayStation.getName().compareTo(name) == 0)

                return railwayStation;

        return null;

    }

    public RailwayUser[] getRailwayUsers() {

        return this.railwayUsers.values().stream().toArray(RailwayUser[]::new);

    }

    public RailwayStation[] getRailwayStations() {

        return super.getNodes().stream().toArray(RailwayStation[]::new);

    }

    public RailwayTrain[] getRailwayTrains() {

        return this.railwayTrains.stream().toArray(RailwayTrain[]::new);

    }

    public DataTranslator<RailwayNetwork> getTranslator() {

        return this.translator;

    }

    public final List<RailwayPath> getAllPaths(RailwayDrive railwayDrive) {

        class BranchRoute {

            private final List<RailwayPath> paths = new ArrayList<RailwayPath>();

            private final Set<RailwayStation> visitedStations = new HashSet<RailwayStation>();

            private final RailwayDrive railwayDrive;

            private final RailwayPath currentPath;

            BranchRoute(RailwayDrive railwayDrive) {

                this.railwayDrive = railwayDrive;

                this.currentPath = new RailwayPath(railwayDrive);

            }

            private void dfs(RailwayStation source, RailwayStation destination) {

                if(this.visitedStations.contains(source))

                    return;

                else if(source == destination) {

                    this.paths.add((RailwayPath) this.currentPath.clone());

                    return;

                }


                this.visitedStations.add(source);

                for(RailwayBranch railwayBranch: source.getBranches()) {

                    if(this.currentPath.setCurrentBranchProps(railwayBranch))

                        this.dfs(railwayBranch.getNextNode(), destination);

                    this.currentPath.resetCurrentBranchProps();

                }

                this.visitedStations.remove(source);

            }

            List<RailwayPath> getPathsUsingDFS() {

                this.dfs(this.railwayDrive.getSource(), this.railwayDrive.getDestination());

                return this.paths;

            }

        }

        return new BranchRoute(railwayDrive).getPathsUsingDFS();

    }

}