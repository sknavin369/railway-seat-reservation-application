package oops.railway.object;

import oops.util.*;

import java.util.*;

import java.io.*;

public final class RailwayPath implements Serializable, Comparable<RailwayPath> {

    private static final long serialVersionUID = 151637475778656700L;

    /* ********************** */

    transient final Stack<Integer> ticketCountStack;

    /* ********************** */

    final Stack<TrainPath> trainPaths = new Stack<TrainPath>();

    private final RailwayDrive railwayDrive;

    /* ********************** */

    private final Integer maxAvailableSeatCount, multipleTrainCount;

    private final TrainPath.Seat maxTicketSeat;

    /* ********************** */

    RailwayPath(RailwayDrive railwayDrive) {

        this.railwayDrive = railwayDrive;

        this.ticketCountStack = new Stack<Integer>() {{

            this.push(Integer.MAX_VALUE);

        }};

        this.maxAvailableSeatCount = this.multipleTrainCount = -1;

        this.maxTicketSeat = null;

    }

    private RailwayPath(RailwayPath railwayPath) { // shallow copy constructor

        this.ticketCountStack = null;

        this.railwayDrive = railwayPath.getRailwayDrive();

        for(TrainPath trainPath: railwayPath.getTrainPaths())

            this.trainPaths.push((TrainPath) trainPath.clone());


        TrainPath.Seat maxTicketSeat = TrainPath.Seat.getDefaultSeat();

        for(TrainPath trainPath: this.getTrainPaths())

            if(maxTicketSeat.compareTo(trainPath.getMaxTicketSeat()) > 0)

                maxTicketSeat = trainPath.getMaxTicketSeat();


        this.maxAvailableSeatCount = railwayPath.ticketCountStack.peek();

        this.multipleTrainCount = this.getTrainPaths().size();

        this.maxTicketSeat = maxTicketSeat;

    }

    /* ********************** */

    boolean setCurrentBranchProps(RailwayBranch currRailwayBranch) {

        if(this.trainPaths.empty() || this.trainPaths.peek().getRailwayTrain() != currRailwayBranch.getRailwayTrain())

            this.trainPaths.push(new TrainPath(currRailwayBranch.getRailwayTrain()));

        this.ticketCountStack.push(Math.min(this.ticketCountStack.peek(), this.trainPaths.peek().addRailwayBranch(currRailwayBranch)));

        return this.ticketCountStack.peek() != 0;

    }

    void resetCurrentBranchProps() {

        this.ticketCountStack.pop();

        if(this.trainPaths.peek().removeRailwayBranch())

            this.trainPaths.pop();

    }

    /* ********************** */

    public Stack<TrainPath> getTrainPaths() {

        return this.trainPaths;

    }

    /* ********************** */

    public int getMaxAvailableSeatCount() {

        return this.maxAvailableSeatCount;

    }

    public int getMultipleTrainCount() {

        return this.multipleTrainCount;

    }

    /* ********************** */

    public TrainPath.Seat getMaxTicketSeat() {

        return this.maxTicketSeat;

    }

    /* ********************** */

    public int getMinimumBookingSeatCount() {

        return Math.min(this.getMaxAvailableSeatCount(), RailwayBranch.MAX_BOOKING_SEATS);

    }

    /* ********************** */

    public RailwayDrive getRailwayDrive() {

        return this.railwayDrive;

    }

    @Override
    public int compareTo(RailwayPath anotherRailwayPath) {

        int maxAvailableSeatCountDiff = this.getMaxAvailableSeatCount() - anotherRailwayPath.getMaxAvailableSeatCount();

        if(maxAvailableSeatCountDiff != 0)

            return -maxAvailableSeatCountDiff;

        return this.getMultipleTrainCount() - anotherRailwayPath.getMultipleTrainCount();

    }

    @Override
    public Object clone() {

        return new RailwayPath(this);

    }

    @Override
    public String toString() {

        return "Maximum Number of Available Tickets: " + this.getMaxAvailableSeatCount() + " | " + (this.getMultipleTrainCount() == 1 ? "Direct Train" : "Multiple Trains") +
               "\n\n" + StringUtil.arrayToString("\n\n", this.getTrainPaths().toArray());

    }

}