package oops.railway.ui;

import oops.io.port.*;

import oops.io.task.*;

import oops.lang.reflect.*;

import oops.railway.object.*;

import oops.railway.validator.*;

public interface RailwayUI {

    default InputInvoker[] getRegisterInvokers() {

        RegistrationValidator registrationValidator = new RegistrationValidator(this.getRailwayNetwork());

        return new InputInvoker[] {
            new StringInvoker("Enter User Name: ", new MethodInvoker(registrationValidator, RegistrationValidator.class,  "validateUserName")),
            new StringInvoker("Enter " + this.getRailwayNetwork().getUIString() + " Number: ", new MethodInvoker(registrationValidator, RegistrationValidator.class,  "validateUINumber")),
            new StringInvoker("Enter E-Mail ID: ", new MethodInvoker(registrationValidator, RegistrationValidator.class,  "validateEMailID")),
            new PasswordInvoker("Enter Password: ", new MethodInvoker(registrationValidator, RegistrationValidator.class,  "validatePassword")),
            new PasswordInvoker("Confirm Password: ", new MethodInvoker(registrationValidator, RegistrationValidator.class,  "confirmPassword"))};

    }

    default InputInvoker[] getLoginInvokers() {

        LoginValidator loginValidator = new LoginValidator(this.getRailwayNetwork());

        return new InputInvoker[] {
            new StringInvoker("Enter User Name: ", new MethodInvoker(loginValidator, LoginValidator.class, "validateUserName")),
            new PasswordInvoker("Enter Password: ", new MethodInvoker(loginValidator, LoginValidator.class, "validatePasswordFor"))};

    }

    default InputInvoker[] getTerminalInvokers() {

        TerminalValidator terminalValidator = new TerminalValidator(this.getRailwayNetwork());

        return new InputInvoker[] {
            new StringInvoker("Enter 'From' Station: ", new MethodInvoker(terminalValidator, TerminalValidator.class, "validateSource")),
            new StringInvoker("Enter 'To' Station: ", new MethodInvoker(terminalValidator, TerminalValidator.class, "validateDestination"))};

    }

    default InputInvoker[] getReservationInvokers() {

        ReservationValidator reservationValidator = new ReservationValidator(this.getRailwayNetwork());

        return new InputInvoker[] {
            new StringInvoker("Passenger's Name: ", new MethodInvoker(reservationValidator, ReservationValidator.class, "validatePassengerName")),
            new StringInvoker("Passenger's " + this.getRailwayNetwork().getUIString() + " Number: ", new MethodInvoker(reservationValidator, ReservationValidator.class, "validateUINumber"))};

    }

    default InputInvoker[] getPNRVerifierInvokersFor(RailwayUser railwayUser) {

        return new InputInvoker[] {
            new StringInvoker("Enter PNR Number of Ticket: ", new MethodInvoker(TicketValidator.class, "validatePNRNumberFor", railwayUser))};

    }

    default InputInvoker[] getPasswordVerifierInvokersFor(RailwayUser railwayUser) {

        return new InputInvoker[] {
            new PasswordInvoker("Confirm Password: ", new MethodInvoker(LoginValidator.class, "verifyPasswordFor", railwayUser))};

    }

    default InputInvoker[] getSeatCancellerInvokersFor(RailwayTicket railwayTicket) {

        return new InputInvoker[] {
             new StringInvoker("Passenger's " + this.getRailwayNetwork().getUIString() + " Number: ", new MethodInvoker(new SeatCancellationValidator(this.getRailwayNetwork(), railwayTicket), SeatCancellationValidator.class, "validateUINumber"))};

    }

    default InputInvoker[] getUserDetailUpdateInvokersFor(RailwayUser railwayUser) {

        UserDetailUpdateValidator userDetailUpdateValidator = new UserDetailUpdateValidator(this.getRailwayNetwork(), railwayUser);

        return new InputInvoker[] {
            new StringInvoker("Enter New User Name: ", new MethodInvoker(userDetailUpdateValidator, UserDetailUpdateValidator.class,  "validateNewUserName")),
            new StringInvoker("Enter New " + this.getRailwayNetwork().getUIString() + " Number: ", new MethodInvoker(userDetailUpdateValidator, UserDetailUpdateValidator.class,  "validateNewUINumber")),
            new StringInvoker("Enter New E-Mail ID: ", new MethodInvoker(userDetailUpdateValidator, UserDetailUpdateValidator.class,  "validateNewEMailID")),
            new PasswordInvoker("Enter New Password: ", new MethodInvoker(userDetailUpdateValidator, UserDetailUpdateValidator.class,  "validateNewPassword"))};

    }


    RailwayNetwork getRailwayNetwork();

    DataTranslator<RailwayNetwork> getTranslator();


    void startUI();

    void displayMainMenu();

    RailwayUser registerNewUser();

    RailwayUser loginAsUser();

    void userLoginFor(RailwayUser railwayUser);

    void bookTicketsFor(RailwayUser railwayUser);

    void doWhenPathsAreAbsent(RailwayUser railwayUser, RailwayDrive railwayDrive);

    void viewTrainDetails();

    void viewStatusOfTicketsFor(RailwayUser railwayUser);

    void showTicketStatusOf(RailwayTicket railwayTicket);

    void updateUserDetailsOf(RailwayUser railwayUser);

    void cancelTicketFor(RailwayUser railwayUser);

    boolean exit();

}