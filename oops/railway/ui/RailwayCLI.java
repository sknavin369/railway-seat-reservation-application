package oops.railway.ui;


import oops.io.port.*;

import oops.io.task.*;

import oops.railway.object.*;

import oops.railway.validator.*;

import oops.util.*;

import oops.lang.*;

import oops.lang.reflect.*;


import java.util.*;

import java.io.*;

import java.security.*;

public class RailwayCLI implements RailwayUI {

    private final RailwayNetwork railwayNetwork;

    private final DataTranslator<RailwayNetwork> translator;

    public RailwayCLI(RailwayNetwork railwayNetwork, DataTranslator<RailwayNetwork> translator) {

        this.railwayNetwork = railwayNetwork;

        this.translator = translator;

    }

    @Override
    public RailwayNetwork getRailwayNetwork() {

        return this.railwayNetwork;

    }

    @Override
    public DataTranslator<RailwayNetwork> getTranslator() {

        return this.translator;

    }

    @Override
    public void startUI() {

        ConsoleUtil.flushConsole();

        ConsoleUtil.waitForPress("\t\t\tWELCOME TO " + this.getRailwayNetwork().getName().toUpperCase() + "!\n" + this.getRailwayNetwork().getWelcomeMessage());

        this.displayMainMenu();

    }

    @Override
    public void displayMainMenu() {

        boolean done = false;

        while(!done)

            switch(ConsoleUtil.getChoosenOption(null, "MAIN MENU", null, "Register as New User", "Login as User", "Exit")) {

                case 1:

                    this.userLoginFor(this.registerNewUser());

                    break;

                case 2:

                    this.userLoginFor(this.loginAsUser());

                    break;

                case 3:

                    done = this.exit();

            }

    }

    @Override
    public RailwayUser registerNewUser() {

        final String[] inputs = ConsoleUtil.getInputsFromInvokers("REGISTER NEW USER", null, null, RailwayUI.super.getRegisterInvokers());

        if(inputs == null)

            return null;


        try {

            return this.getRailwayNetwork().getNewRailwayUser(Arrays.copyOfRange(inputs, 0, inputs.length-1));

        }

        catch(InvalidParameterException exception) {

            exception.printStackTrace();

        }

        catch(IllegalArgumentException exception) {

            ConsoleUtil.waitForPress("\nThis User Name has been selected just now! Please Try Again!");

        }

        catch(IllegalAccessException exception) {

            exception.printStackTrace();

        }

        return null;

    }

    @Override
    public RailwayUser loginAsUser() {

        final String[] inputs = ConsoleUtil.getInputsFromInvokers("LOG IN", null, null, RailwayUI.super.getLoginInvokers());

        if(inputs == null)

            return null;


        return this.getRailwayNetwork().getUserByName(inputs[0]);

    }

    @Override
    public void userLoginFor(RailwayUser railwayUser) {

        if(railwayUser == null)

            return;


        boolean done = false;

        while(!done)

            switch(ConsoleUtil.getChoosenOption(null, "LOGIN MENU | " + railwayUser.info.userName, null, "Book Tickets", "View Train Details", "View Status of Tickets", "Update User Details", "Cancel Booked Tickets", "Return Back to Main Menu")) {

                case 1:

                    this.bookTicketsFor(railwayUser);

                    break;

                case 2:

                    this.viewTrainDetails();

                    break;

                case 3:

                    this.viewStatusOfTicketsFor(railwayUser);

                    break;

                case 4:

                    this.updateUserDetailsOf(railwayUser);

                    break;

                case 5:

                    this.cancelTicketFor(railwayUser);

                    break;

                case 6:

                    done = true;

            }

    }

    @Override
    public void bookTicketsFor(RailwayUser railwayUser) {

        final String[] stations = ConsoleUtil.getInputsFromInvokers("BOOK TRAINS", new Message<String>("The Available Stations in '" + this.getRailwayNetwork().getName().toUpperCase() + "' are:", ArraysUtil.getToStringArray(this.getRailwayNetwork().getRailwayStations())), null, RailwayUI.super.getTerminalInvokers());

        if(stations == null)

            return;


        final RailwayDrive railwayDrive = new RailwayDrive(this.getRailwayNetwork().getStationByname(stations[0]), this.getRailwayNetwork().getStationByname(stations[1]));



        final List<RailwayPath> pathList = this.railwayNetwork.getAllPaths(railwayDrive);

        if(pathList.size() == 0) {

            this.doWhenPathsAreAbsent(railwayUser, railwayDrive);

            return;

        }


        final RailwayPath[] pathArray = pathList.stream().toArray(RailwayPath[]::new);

        Arrays.sort(pathArray);



        final int index = ConsoleUtil.getChoosenOption('_', "THE AVAILABLE TRAINS ARE:", "-1", (Object[]) pathArray) - 1;

        if(~index == 0)

            return;



        final RailwayPath chosenPath = pathArray[index];

        final TrainPath.Seat seat = chosenPath.getMaxTicketSeat();

        final Integer maxAvailableSeats = chosenPath.getMinimumBookingSeatCount();

        final Integer totalSeats = seat.getTotalSeats(), confirmableSeats = seat.getConfirmable(), waitableSeats = seat.getWaitable();


        if(confirmableSeats < maxAvailableSeats && waitableSeats != 0)

            if(!ConsoleUtil.getYesOrNoResponse((chosenPath.getMultipleTrainCount() != 1 ? "Atleast, in one train, out" : "Out") + " of " +
                                               StringUtil.getStringWithSuffixForCount("seat", maxAvailableSeats) + ",\n\t" +
                                               StringUtil.getStringWithSuffixForCount("seat", Math.max(maxAvailableSeats - waitableSeats, confirmableSeats)) + " will be confirmed, and\n\t" +
                                               StringUtil.getStringWithSuffixForCount("seat", Math.min(maxAvailableSeats - confirmableSeats, waitableSeats)) + " will be in waiting list."))

                return;



        final InputInvoker[] inputInvokers = ConsoleUtil.getInputsFromRepetitiveInvokers("PASSENGER DETAILS", new Message<String>("Enter a maximum of " + Integer.toString(maxAvailableSeats) + " Passenger Details", null), null, maxAvailableSeats, RailwayUI.super.getReservationInvokers());

        if(inputInvokers == null)

            return;


        final Map<Long, String> passengerMap = ((ReservationValidator) (inputInvokers[0].getInputValidator().getObject())).getPassengers();

        final Set<RailwayPassenger> passengers = RailwayPassenger.getRailwayPassengersFrom(passengerMap);



        if(passengers.isEmpty())

            return;



        final RailwayTicket railwayTicket = this.getRailwayNetwork().getNewTicket(railwayUser, chosenPath, passengers);


        ConsoleUtil.waitForPress("\n\n\t\tBOOKING SUCCESSFUL!");


        this.showTicketStatusOf(railwayTicket);

    }

    @Override
    public void doWhenPathsAreAbsent(RailwayUser railwayUser, RailwayDrive railwayDrive) {

        ConsoleUtil.waitForPress("\t\tSORRY FOR THE INCONVENIENCE!\n\n\t NO TRAINS ARE AVAILABLE FROM '" + railwayDrive.getSource() + "' TO: '" + railwayDrive.getDestination() + "'!");

    }

    @Override
    public void viewTrainDetails() {

        ConsoleUtil.waitForPress("\t\tTHE AVAILABLE TRAINS ARE:\n\n\n" + StringUtil.getItemsInOrder("\n" + StringUtil.repeat('_', 120) + "\n\n", (Object[]) this.getRailwayNetwork().getRailwayTrains()) + "\n");

    }

    @Override
    public void viewStatusOfTicketsFor(RailwayUser railwayUser) {

        final String[] inputs = ConsoleUtil.getInputsFromInvokers("VIEW STATUS OF TICKETS", null, null, RailwayUI.super.getPNRVerifierInvokersFor(railwayUser));

        if(inputs == null)

            return;


        this.showTicketStatusOf(railwayUser.getTicketByPNRNumber(Long.parseLong(inputs[0])));

    }

    @Override
    public void showTicketStatusOf(RailwayTicket railwayTicket) {

        ConsoleUtil.waitForPress(RailwaySystem.getStringOfTicketStatusFor(this.getRailwayNetwork(), railwayTicket));

    }

    @Override
    public void updateUserDetailsOf(RailwayUser railwayUser) {

        if(ConsoleUtil.getInputsFromInvokers("CONFIRM PASSWORD", null, null, RailwayUI.super.getPasswordVerifierInvokersFor(railwayUser)) == null)

            return;


        int option;

        String choice;

        final InputInvoker[] userDetailUpdateInvokers = RailwayUI.super.getUserDetailUpdateInvokersFor(railwayUser);

        final String[] choices = new String[] {"Reset User Name", "Reset " + this.getRailwayNetwork().getUIString() + " Number", "Reset E-Mail ID", "Reset Password", "Return Back to Login Menu"};

        while((option = ConsoleUtil.getChoosenOption(null, "UPDATE USER DETAILS | " + railwayUser.info.userName, null, (Object[]) choices)) != 5)

            if(ConsoleUtil.getInputsFromInvokers("UPDATE " + (choice = choices[option-1].substring(choices[option-1].indexOf(' ') + 1).toUpperCase()), null, null, userDetailUpdateInvokers[option-1]) != null)

                ConsoleUtil.waitForPress("\n\n\t\t" + choice + " UPDATED SUCCESSFULLY!");

    }

    @Override
    public void cancelTicketFor(RailwayUser railwayUser) {

        if(ConsoleUtil.getInputsFromInvokers("CONFIRM PASSWORD", null, null, RailwayUI.super.getPasswordVerifierInvokersFor(railwayUser)) == null)

            return;


        final String[] inputs = ConsoleUtil.getInputsFromInvokers("CANCEL SEATS", null, null, RailwayUI.super.getPNRVerifierInvokersFor(railwayUser));

        if(inputs == null)

            return;



        final RailwayTicket railwayTicket = railwayUser.getTicketByPNRNumber(Long.parseLong(inputs[0]));


        Set<RailwayPassenger> passengers = new HashSet<RailwayPassenger>(railwayTicket.getPassengers());


        if(!ConsoleUtil.getYesOrNoResponseFor("Do you want to cancel the seats of all the passengers?")) {


            final InputInvoker[] inputInvokers = ConsoleUtil.getInputsFromRepetitiveInvokers("ENTER PASSENGER DETAILS TO CANCEL SEATS", new Message<String>("Enter a maximum of " + Integer.toString(railwayTicket.getSeatCount()) + " Passenger Details", null), null, railwayTicket.getSeatCount(), RailwayUI.super.getSeatCancellerInvokersFor(railwayTicket));

            if(inputInvokers == null)

                return;


            passengers = ((SeatCancellationValidator) (inputInvokers[0].getInputValidator().getObject())).getPassengers();

            if(passengers.isEmpty())

                return;

        }


        final boolean cancelledAllSeats = railwayTicket.cancelSeatsFor(passengers);

        ConsoleUtil.waitForPress("\n\n\t\tSEATS CANCELLED SUCCESSFULLY!");


        if(!cancelledAllSeats)

            this.showTicketStatusOf(railwayTicket);

    }

    @Override
    public boolean exit() {

        return ConsoleUtil.getYesOrNoResponseFor("Do you want to Exit?");

    }

}