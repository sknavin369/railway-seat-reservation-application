package oops.graph.lang;

import oops.graph.util.*;

import java.util.*;

public class AdjacenyListGraph<T extends Node> implements AbstractAdjacencyList {

    private final String name;

    private final Set<T> nodeSet;

    public AdjacenyListGraph(String name) {

        this.nodeSet = new HashSet<T>();

        this.name = name;

    }

    public String getName() {

        return this.name;

    }

    public void addNodes(T[] nodes) {

        Arrays.stream(nodes).forEach(this.nodeSet::add);

    }

    public Set<T> getNodes() {

        return this.nodeSet;

    }

}