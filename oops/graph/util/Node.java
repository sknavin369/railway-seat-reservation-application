package oops.graph.util;

import java.util.*;

import java.io.*;

public abstract class Node implements Serializable {

    private static final long serialVersionUID = -1336316291759610L;

    private final String name;

    public Node(String name) {

        this.name = name;

    }

    public String getName() {

        return this.name;

    }

}