package oops.graph.util;

import oops.graph.lang.*;

import java.util.*;
import java.io.*;

public class AdjacencyListBranch<T extends AdjacencyListNode> extends Branch {

    private final Graph.TerminalPair<T> terminal;

    public AdjacencyListBranch(String name, Graph.TerminalPair<T> terminal) {

        super(name);

        this.terminal = terminal;

    }

    public Graph.TerminalPair<T> getTerminal() {

        return this.terminal;

    }

    public T getPrevNode() {

        return this.getTerminal().getSource();

    }

    public T getNextNode() {

        return this.getTerminal().getDestination();

    }

    @Override
    public String toString() {

        return this.getTerminal().toString();

    }

}