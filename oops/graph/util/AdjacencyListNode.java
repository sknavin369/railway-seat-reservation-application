package oops.graph.util;

import oops.graph.lang.*;

import java.util.*;

public class AdjacencyListNode<T extends AdjacencyListBranch> extends Node {

    private static final long serialVersionUID = 3274024164500095944L;

    private final Set<T> branches = new HashSet<T>();

    public AdjacencyListNode(String name) {
        
        super(name);

    }

    public void addBranch(T branch) {

        this.branches.add(branch);

    }

    public void printBranches() {

        this.branches.stream().forEach(System.out::println);

    }

    public Set<T> getBranches() {

        return this.branches;

    }

}