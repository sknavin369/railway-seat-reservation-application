package oops.railway.networks;

import oops.railway.object.*;

import oops.io.port.*;

import oops.lang.*;

import java.util.*;

public class IndianRailwayNetwork extends RailwayNetwork {


    private static final String[] stationNames = new String[] {"Madhura", "Ayodhya", "Varanasi", "Kolkata", "Bangalore",
                                                               "Chennai", "Coimbatore", "Pune", "Mumbai", "Delhi"};

    private static final Map<String, String[]> trainDetails = new HashMap<String, String[]>() {{
        this.put("Chennai Express",        new String[] {stationNames[5], stationNames[4], stationNames[2], stationNames[1], stationNames[0]});
        this.put("P2B Express",            new String[] {stationNames[7], stationNames[8], stationNames[3], stationNames[5], stationNames[6], stationNames[4]});
        this.put("Madhura Express",        new String[] {stationNames[0], stationNames[2], stationNames[1], stationNames[8], stationNames[5], stationNames[6]});
        this.put("C2M Super-Fast Express", new String[] {stationNames[5], stationNames[3], stationNames[1], stationNames[0]});
        this.put("North Express",          new String[] {stationNames[0], stationNames[4], stationNames[2], stationNames[1], stationNames[7]});
        this.put("East Express",           new String[] {stationNames[7], stationNames[6], stationNames[8], stationNames[9], stationNames[3]});}};


    public IndianRailwayNetwork(String networkName, DataTranslator<RailwayNetwork> translator) {

        super(networkName, translator, IndianRailwayNetwork.stationNames, IndianRailwayNetwork.trainDetails);

    }

}