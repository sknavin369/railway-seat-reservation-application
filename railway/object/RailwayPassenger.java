package oops.railway.object;

import java.io.*;

import java.util.*;

public final class RailwayPassenger implements Serializable, Comparable<RailwayPassenger> {

    private static final long serialVersionUID = 1516374757496656799L;

    /* ********************** */

    private final String name;

    private final Long aadharNumber;

    private RailwayTicket railwayTicket;

    public RailwayPassenger(String name, Long aadharNumber) {

        this.name = name;

        this.aadharNumber = aadharNumber;

    }

    public String getName() {

        return this.name;

    }

    public long getAadharNumber() {

        return this.aadharNumber;

    }

    /* ********************** */

    void setRailwayTicket(RailwayTicket railwayTicket) {

        this.railwayTicket = railwayTicket;

    }

    RailwayTicket getRailwayTicket() {

        return this.railwayTicket;

    }

    public long getPNRNumber() {

        return this.railwayTicket.getPNRNumber();

    }

    /* ********************** */

    public String getPassengerDetail() {

        return "Name: " + this.name + " | Aadhar Number: " + Long.toString(this.aadharNumber);

    }

    /* ********************** */

    public static Set<RailwayPassenger> getRailwayPassengersFrom(Map<Long, String> passengers) {

        Set<RailwayPassenger> passengerSet = new LinkedHashSet<RailwayPassenger>();

        for(Map.Entry<Long, String> entry: passengers.entrySet())

            passengerSet.add(new RailwayPassenger(entry.getValue(), entry.getKey()));

        return passengerSet;

    }

    /* ********************** */

    @Override
    public int compareTo(RailwayPassenger anotherPassenger) {

        int strCompare = this.getName().compareTo(anotherPassenger.getName());

        if(strCompare != 0)

            return strCompare;

        return (int) (this.getAadharNumber() - anotherPassenger.getAadharNumber());

    }

    @Override
    public String toString() {

        return "PNR Number: " + this.getPNRNumber() + " | " + this.getPassengerDetail();

    }

}