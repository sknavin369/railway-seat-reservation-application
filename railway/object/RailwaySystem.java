package oops.railway.object;

import oops.io.port.*;

import oops.io.task.*;

import oops.util.*;

import java.util.*;

import java.io.*;

public final class RailwaySystem {

    private static final String userTicketPath = "C:/Users/user/Desktop/oops/Railway Tickets/";

    static final String networkPath = "C:/Users/user/Desktop/oops/railway/saved_networks/";

    private RailwaySystem() {}

    public static RailwayNetwork getNewOrExistingNetwork(String networkName, DataTranslator<RailwayNetwork> translator, Class<?> classOfNetwork) {

        final File networkFile = new File(RailwaySystem.networkPath + networkName);

        RailwayNetwork railwayNetwork = null;

        if(networkFile.exists())

            railwayNetwork = translator.retrieve(networkFile);

        else {

            IOUtil.mkdirs(RailwaySystem.userTicketPath + networkName);

            try {

                railwayNetwork = (RailwayNetwork) classOfNetwork.getDeclaredConstructor(String.class, DataTranslator.class).newInstance(networkName, translator);

            }

            catch(Exception exception) {

                exception.printStackTrace();

            }

        }

        return railwayNetwork;

    }

    static void saveExistingNetwork(RailwayNetwork railwayNetwork, DataTranslator<RailwayNetwork> translator) {

        translator.save(railwayNetwork, railwayNetwork.dataFile);

    }

    public static String getStringOfTicketStatusFor(RailwayNetwork railwayNetwork, RailwayTicket railwayTicket) {


        final RailwayUser.Info info = railwayTicket.getUserInfo();


        String text = "\t\t\t\t" + railwayNetwork.getName().toUpperCase() +
                      "\n\nUser Name: " + info.userName +
                      "\n\nAadhar Number: " + Long.toString(info.aadharNumber) +
                      "\n\nE-Mail Id: " + info.eMailID +
                      "\n\n\n" + railwayTicket.getRailwayDrive().toString() +
                      "\n\n\nPNR Number: " + Long.toString(railwayTicket.getPNRNumber()) +
                      "\n\n\n\t\t\tSTATUS OF SEAT(S) AT EACH STATIONS";


        for(TrainPath trainPath: railwayTicket.getRailwayPath().getTrainPaths()) {


            text += "\n" + StringUtil.repeat('_', 120) + "\n\n" + trainPath.getRailwayTrain().getName();

            for(RailwayBranch railwayBranch: trainPath.getRailwayBranches())

                text += "\n\n\t" + railwayBranch.getName();



            final RailwayBranch railwayBranch = trainPath.getRailwayBranches().peek();



            if(!railwayBranch.getPassengerSeatMap().isEmpty())

                for(Map.Entry<RailwayPassenger, Integer> entry: railwayBranch.getPassengerSeatMap().entrySet())

                    if(entry.getKey().getPNRNumber() == railwayTicket.getPNRNumber())

                        text += "\n\n\t\t" + entry.getKey().getPassengerDetail() + " | Seat Number: " + Integer.toString(entry.getValue());



            if(!railwayBranch.getWaitingSet().isEmpty())

                for(RailwayPassenger railwayPassenger: railwayBranch.getWaitingSet())

                    if(railwayPassenger.getPNRNumber() == railwayTicket.getPNRNumber())

                        text += "\n\n\t\t" + railwayPassenger.getPassengerDetail() + " | In Waiting List!";

        }

        return text;

    }

    static void setTicketStatusToFile(RailwayNetwork railwayNetwork, RailwayTicket railwayTicket) {

        final String text = RailwaySystem.getStringOfTicketStatusFor(railwayNetwork, railwayTicket),

                     userName = railwayTicket.getUserInfo().userName;

        final Long pnrNumber = railwayTicket.getPNRNumber();


        try {

            IOUtil.writeTextTo(RailwaySystem.userTicketPath + railwayNetwork.getName() + "/" + userName + "/" + Long.toString(pnrNumber) + ".txt", text);

        }

        catch(FileNotFoundException exception) {

            new File(RailwaySystem.userTicketPath + railwayNetwork.getName() + "/" + userName).mkdirs();

            RailwaySystem.setTicketStatusToFile(railwayNetwork, railwayTicket);

        }

        catch(IOException exception) {

            exception.printStackTrace();

        }

    }

    public static boolean renameTicketFileTo(RailwayNetwork railwayNetwork, RailwayUser railwayUser, String newUserName) {

        return FileManipulator.renameFile(RailwaySystem.userTicketPath + railwayNetwork.getName() + "/", railwayUser.info.userName, newUserName);

    }

    public static void deleteNetwork(RailwayNetwork railwayNetwork) {

        IOUtil.deleteFile(RailwaySystem.networkPath + railwayNetwork.getName());

        IOUtil.deleteDir(RailwaySystem.userTicketPath + railwayNetwork.getName());

    }

    public static void deleteTicketInFile(RailwayNetwork railwayNetwork, String userName, Long pnrNumber) {

        IOUtil.deleteFileAndDirIfEmpty(Long.toString(pnrNumber) + ".txt", RailwaySystem.userTicketPath + railwayNetwork.getName() + "/" + userName);

    }

}