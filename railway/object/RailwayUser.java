package oops.railway.object;

import java.util.*;

import java.io.*;

import java.security.*;

import java.util.regex.*;

public final class RailwayUser implements Serializable {

    private static final long serialVersionUID = 1698267733479849723L;

    public final RailwayUser.Info info;

    private final RailwayNetwork railwayNetwork;


    private final Map<Long, RailwayTicket> ticketMap = new HashMap<Long, RailwayTicket>();



    public RailwayUser(RailwayNetwork railwayNetwork, RailwayUser.Info info) throws IllegalAccessException, IllegalArgumentException {

        if(railwayNetwork.searchUserByName(info.userName))

            throw new IllegalArgumentException("The User Name has been taken recently! Try with some other User Name.");


//         StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];

//         if(stackTraceElement.getMethodName() != "registerNewUser" || stackTraceElement.getClassName().substring(0, 11).compareTo("railway.ui.") != 0)

//             throw new IllegalAccessException("Access Denied! Cannot get new Railway User.");

        this.railwayNetwork = railwayNetwork;

        this.info = info;

    }

    public RailwayNetwork getRailwayNetwork() {

        return this.railwayNetwork;

    }

    void addTicket(RailwayTicket railwayTicket) {

        this.ticketMap.put(railwayTicket.getPNRNumber(), railwayTicket);

        this.getRailwayNetwork().addTicket(railwayTicket);

    }

    void removeTicket(RailwayTicket railwayTicket) {

        this.ticketMap.remove(railwayTicket.getPNRNumber());

        this.getRailwayNetwork().removeTicket(railwayTicket);

    }

    public RailwayTicket getTicketByPNRNumber(long pnrNumber) {

        return this.ticketMap.get(pnrNumber);

    }

    public void updateUserName(String newUserName) {

        RailwaySystem.renameTicketFileTo(this.getRailwayNetwork(), this, newUserName);

        this.getRailwayNetwork().removeUsers(this);

        this.info.resetUserName(newUserName);

        this.getRailwayNetwork().addUsers(this);

        this.updateTicketsInFile();

    }

    public void updateAadharNumber(long aadharNumber) {

        this.info.resetAadharNumber(aadharNumber);

        RailwaySystem.saveExistingNetwork(this.getRailwayNetwork(), this.getRailwayNetwork().getTranslator());

        this.updateTicketsInFile();

    }

    public void updateEMailID(String eMailID) {

        this.info.resetEMailID(eMailID);

        RailwaySystem.saveExistingNetwork(this.getRailwayNetwork(), this.getRailwayNetwork().getTranslator());

        this.updateTicketsInFile();

    }

    public void updatePassword(String password) {

        this.info.resetPassword(password);

        RailwaySystem.saveExistingNetwork(this.getRailwayNetwork(), this.getRailwayNetwork().getTranslator());

    }

    private void updateTicketsInFile() {

        for(RailwayTicket railwayTicket: this.ticketMap.values())

            RailwaySystem.setTicketStatusToFile(this.getRailwayNetwork(), railwayTicket);

    }

    public static class Info implements Serializable {

        private static final long serialVersionUID = 4480263778847664203L;

        private static final Pattern passwordPattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,20}$");

        public String userName;

        public long aadharNumber;

        public String eMailID;

        private String password;

        public Info(String userName, long aadharNumber, String eMailID, String password) {

            this.userName = userName;

            this.aadharNumber = aadharNumber;

            this.eMailID = eMailID;

            this.password = password;

        }

        void resetUserName(String userName) {

            this.userName = userName;

        }

        void resetAadharNumber(long aadharNumber) {

            this.aadharNumber = aadharNumber;

        }

        void resetEMailID(String eMailID) {

            this.eMailID = eMailID;

        }

        void resetPassword(String password) {

            this.password = password;

        }

        public boolean verifyPassword(String password) {

            return this.password.compareTo(password) == 0;

        }

        public static boolean validatePassword(String password) {

            Matcher m = RailwayUser.Info.passwordPattern.matcher(password);

            return m.matches();

        }

        @Override
        public String toString() {

            return "RailwayUser.Info[userName=" + this.userName + ", aadharNumber=" + this.aadharNumber + ", eMailID=" + this.eMailID + "]";

        }

    }

    @Override
    public String toString() {

        return this.info.toString();

    }

}