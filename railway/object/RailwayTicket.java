package oops.railway.object;

import java.util.*;

import java.util.stream.*;

import java.io.*;

public final class RailwayTicket implements Serializable {

    private static final long serialVersionUID = -4572126312014035575L;

    private final long pnrNumber;

    private final RailwayUser railwayUser;

    private final RailwayDrive railwayDrive;

    private final RailwayPath railwayPath;

    private final Set<RailwayPassenger> railwayPassengers;


//      As JVM allows only one thread to access the constructor, toEpochMilli() will be unique for every ticket
    RailwayTicket(RailwayUser railwayUser, RailwayPath railwayPath, Set<RailwayPassenger> railwayPassengers) {

        this.railwayUser = railwayUser;

        this.railwayDrive = railwayPath.getRailwayDrive();

        this.railwayPath = railwayPath;

        this.railwayPassengers = railwayPassengers;

        this.pnrNumber = java.time.ZonedDateTime.now().toInstant().toEpochMilli();


        this.getPassengers().stream().forEach(railwayPassenger -> railwayPassenger.setRailwayTicket(this));


        Integer seatNumber;

        Set<Integer> availableTicketSet;

        Iterator<Integer> iterator;

        for(TrainPath trainPath: this.getRailwayPath().getTrainPaths()) {

            availableTicketSet = trainPath.getAvailableTicketSet();

            iterator = availableTicketSet.iterator();

            for(RailwayPassenger railwayPassenger: this.railwayPassengers) {

                seatNumber = iterator.hasNext() ? iterator.next() : -1;

                for(RailwayBranch railwayBranch: trainPath.getRailwayBranches())

                    railwayBranch.setSeatForPassenger(railwayPassenger, seatNumber);

            }

        }

        this.getRailwayUser().addTicket(this);

    }

    public RailwayDrive getRailwayDrive() {

        return this.railwayDrive;

    }

    public RailwayUser getRailwayUser() {

        return this.railwayUser;

    }

    public RailwayPath getRailwayPath() {

        return this.railwayPath;

    }

    public long getPNRNumber() {

        return this.pnrNumber;

    }

    public int getSeatCount() {

        return this.railwayPassengers.size();

    }

    public RailwayUser.Info getUserInfo() {

        return this.railwayUser.info;

    }

    public Set<RailwayPassenger> getPassengers() {

        return this.railwayPassengers;

    }

    public RailwayPassenger getPassengerByAadharNumber(long aadharNumber) {

        for(RailwayPassenger railwayPassenger: this.railwayPassengers)

            if(railwayPassenger.getAadharNumber() == aadharNumber)

                return railwayPassenger;

        return null;

    }

// returns true, if all passengers seats have been cancelled
    public boolean cancelSeatsFor(Set<RailwayPassenger> cancellingPassengers) {

        this.getPassengers().removeAll(cancellingPassengers);


        Integer seatNumber;

        Set<Integer> availableTicketSet;

        final RailwayNetwork railwayNetwork = this.getRailwayUser().getRailwayNetwork();

        final Set<RailwayPassenger> remainingPassengers = new HashSet<RailwayPassenger>();



        for(TrainPath trainPath: this.getRailwayPath().getTrainPaths())

            if(trainPath.cancelSeatsFor(cancellingPassengers))

                for(RailwayBranch railwayBranch: trainPath.getRailwayBranches())

                    remainingPassengers.addAll(railwayBranch.getWaitingSet());



        for(RailwayPassenger railwayPassenger: remainingPassengers) {

            for(TrainPath trainPath: railwayPassenger.getRailwayTicket().getRailwayPath().getTrainPaths()) {

                Stack<RailwayBranch> railwayBranches = trainPath.getRailwayBranches();

                availableTicketSet = null;

                if(railwayBranches.peek().getSeatForPassenger(railwayPassenger) == null) {

                    for(RailwayBranch railwayBranch: railwayBranches) {

                        if(availableTicketSet == null)

                            availableTicketSet = new HashSet<Integer>(railwayBranch.getTicketSet());

                        else

                            availableTicketSet.retainAll(railwayBranch.getTicketSet());

                    }

                }

                if(availableTicketSet != null && !availableTicketSet.isEmpty()) {

                    seatNumber = availableTicketSet.stream().findFirst().get();

                    for(RailwayBranch railwayBranch: railwayBranches)

                        railwayBranch.setSeatForPassenger(railwayPassenger, seatNumber);

                }

            }

        }


        for(RailwayTicket railwayTicket: remainingPassengers.stream().map(railwayPassenger -> railwayPassenger.getRailwayTicket()).collect(Collectors.toSet()))

            RailwaySystem.setTicketStatusToFile(railwayNetwork, railwayTicket);



        if(this.getPassengers().isEmpty()) {

            this.getRailwayUser().removeTicket(this);

            RailwaySystem.deleteTicketInFile(railwayNetwork, this.getRailwayUser().info.userName, this.getPNRNumber());

            return true;

        }

        RailwaySystem.saveExistingNetwork(railwayNetwork, railwayNetwork.getTranslator());

        RailwaySystem.setTicketStatusToFile(railwayNetwork, this);

        return false;

    }

}