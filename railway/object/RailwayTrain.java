package oops.railway.object;

import oops.graph.lang.*;

import java.util.*;

import java.io.*;

public final class RailwayTrain implements Serializable {

    private static final long serialVersionUID = 845363162917577610L;

    private final List<RailwayBranch> branches = new ArrayList<RailwayBranch>();

    private final RailwayNetwork railwayNetwork;

    private final String name;

    RailwayTrain(RailwayNetwork railwayNetwork, String name, RailwayStation... railwayStations) {

        this.name = name;

        this.railwayNetwork = railwayNetwork;

        RailwayStation prevStation = null;

        RailwayBranch currentBranch = null;

        for(RailwayStation currentStation: railwayStations) {

            if(prevStation != null) {

                currentBranch = new RailwayBranch(this, new Graph.TerminalPair<RailwayStation>(prevStation, currentStation));

                prevStation.addBranch(currentBranch);

                this.branches.add(currentBranch);

            }

            prevStation = currentStation;

        }

    }

    public String getName() {

        return this.name;

    }

    public List<RailwayBranch> getBranches() {

        return this.branches;

    }

    @Override
    public String toString() {

        String ans = this.getName();

        for(RailwayBranch railwayBranch: this.getBranches()) {

            ans += "\n\n\t" + railwayBranch.getName();

            if(!railwayBranch.getPassengerSeatMap().isEmpty())

                for(Map.Entry<RailwayPassenger, Integer> entry: railwayBranch.getPassengerSeatMap().entrySet())

                    ans += "\n\n\t\t" + entry.getKey().toString() + " | Seat Number: " + Integer.toString(entry.getValue());

            if(!railwayBranch.getWaitingSet().isEmpty())

                for(RailwayPassenger railwayPassenger: railwayBranch.getWaitingSet())

                    ans += "\n\n\t\t" + railwayPassenger.toString() + " | In Waiting List!";

        }

        return ans;

    }

}