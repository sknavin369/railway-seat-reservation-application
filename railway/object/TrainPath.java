package oops.railway.object;

import oops.util.*;

import oops.graph.lang.*;

import oops.graph.util.*;

import java.util.*;

import java.io.*;

public final class TrainPath implements Serializable {

    private static final long serialVersionUID = 1516332757496656701L;

    /* ********************** */

    transient final Stack<Set<Integer>> ticketStack;

    transient final Stack<Integer> minWaitingCountStack;

    /* ********************** */

    private final Stack<RailwayBranch> branchStack = new Stack<RailwayBranch>();

    /* ********************** */

    private final Set<Integer> availableTicketSet;

    private final RailwayTrain railwayTrain;

    private final TrainPath.Seat maxTicketSeat;

    /* ********************** */

    public TrainPath(RailwayTrain railwayTrain) {

        this.railwayTrain = railwayTrain;

        this.ticketStack = new Stack<Set<Integer>>();

        this.minWaitingCountStack = new Stack<Integer>() {{

            this.push(Integer.MAX_VALUE);

        }};

        this.maxTicketSeat = null;

        this.availableTicketSet = null;

    }

    private TrainPath(TrainPath trainPath) {

        this.railwayTrain = trainPath.getRailwayTrain();

        for(RailwayBranch railwayBranch: trainPath.getRailwayBranches())

            this.branchStack.push(railwayBranch);

        this.availableTicketSet = new HashSet<Integer>(trainPath.ticketStack.peek());

        this.maxTicketSeat = new TrainPath.Seat(this.availableTicketSet.size(), trainPath.minWaitingCountStack.peek());

        this.ticketStack = null;

        this.minWaitingCountStack = null;

    }

    /* ********************** */

    public static class Seat implements Serializable, Comparable<Seat> {

        private static final long serialVersionUID = 1516332757496656701L;

        private final int confirmable, waitable;

        public Seat(int confirmable, int waitable) {

            this.confirmable = confirmable;

            this.waitable = waitable;

        }

        public int getConfirmable() {

            return this.confirmable;

        }

        public int getWaitable() {

            return this.waitable;

        }

        public int getTotalSeats() {

            return this.getConfirmable() + this.getWaitable();

        }

        public static Seat getDefaultSeat() {

            return new TrainPath.Seat(Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 2);

        }

        @Override
        public int compareTo(Seat anotherSeat) {

            int waitableSeatsDiff = this.getWaitable() - anotherSeat.getWaitable();

            if(waitableSeatsDiff != 0)

                return waitableSeatsDiff;

            return this.getTotalSeats() - anotherSeat.getTotalSeats();

        }

        @Override
        public String toString() {

            return "Confirmable: " + Integer.toString(this.getConfirmable()) + ", Waitable: " + Integer.toString(this.getWaitable());

        }

    }

    /* ********************** */

    int addRailwayBranch(RailwayBranch railwayBranch) {

        this.branchStack.push(railwayBranch);


        if(ticketStack.empty())

            this.ticketStack.push(new HashSet<Integer>(railwayBranch.getTicketSet()));

        else

            this.ticketStack.push(new HashSet<Integer>(this.ticketStack.peek()) {{

                this.retainAll(railwayBranch.getTicketSet());

            }});


        this.minWaitingCountStack.push(Math.min(this.minWaitingCountStack.peek(), railwayBranch.getWaitingCount()));

        return this.ticketStack.peek().size() + this.minWaitingCountStack.peek();

    }

    boolean removeRailwayBranch() {

        this.branchStack.pop();

        this.ticketStack.pop();

        this.minWaitingCountStack.pop();

        return this.isEmpty();

    }

    boolean isEmpty() {

        return this.branchStack.isEmpty();

    }

    /* ********************** */

    public TrainPath.Seat getMaxTicketSeat() {

        return this.maxTicketSeat;

    }

// returns true if atleast one seat has been removed from booked list
    public boolean cancelSeatsFor(Set<RailwayPassenger> cancellingPassengers) {

        boolean removedFromBookedSeats = false;

        for(RailwayBranch railwayBranch: this.getRailwayBranches())

            for(RailwayPassenger railwayPassenger: cancellingPassengers)

                removedFromBookedSeats |= railwayBranch.removeSeatForPassenger(railwayPassenger);

        return removedFromBookedSeats;

    }

    /* ********************** */

    public RailwayTrain getRailwayTrain() {

        return this.railwayTrain;

    }

    public Stack<RailwayBranch> getRailwayBranches() {

        return this.branchStack;

    }

    /* ********************** */

    public Set<Integer> getAvailableTicketSet() {

        return this.availableTicketSet;

    }

    /* ********************** */

    @Override
    public Object clone() {

        return new TrainPath(this);

    }

    @Override
    public String toString() {

        return this.getRailwayTrain().getName() + "\n\n\t" + StringUtil.arrayToString("\n\n\t", this.getRailwayBranches().toArray());

    }

}