package oops.railway.validator;

import oops.railway.object.*;

import java.util.*;

import java.util.regex.*;

import java.security.*;

public final class TerminalValidator {

    private final RailwayNetwork railwayNetwork;

    private String source;

    public TerminalValidator(RailwayNetwork railwayNetwork) {

        this.railwayNetwork = railwayNetwork;

    }

    public void validateSource(String source) throws IllegalArgumentException {

        if(this.railwayNetwork.getStationByname(source) == null)

            throw new IllegalArgumentException("INVALID STATION NAME! ENTER A VALID STATION NAME!");

        this.source = source;

    }

    public void validateDestination(String destination) throws IllegalArgumentException {

        if(this.source.equals(destination))

            throw new IllegalArgumentException("INVALID STATION NAME! THE STATION NAME COINCIDES WITH THE 'FROM' STATION!");

        else if(this.railwayNetwork.getStationByname(destination) == null)

            throw new IllegalArgumentException("INVALID STATION NAME! ENTER A VALID STATION NAME!");

    }

}