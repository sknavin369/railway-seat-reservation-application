package oops.railway.validator;

import oops.railway.object.*;

import oops.railway.networks.*;

import java.util.*;

import java.util.regex.*;

public final class RegistrationValidator {

    private final RailwayNetwork railwayNetwork;

    private String password;

    public RegistrationValidator(RailwayNetwork railwayNetwork) {

        this.railwayNetwork = railwayNetwork;

    }

    public void validateUserName(String userName) throws IllegalArgumentException {

        if(this.railwayNetwork.searchUserByName(userName))

            throw new IllegalArgumentException("USER NAME ALREADY EXISTS IN THIS NETWORK!");

        else if(!userName.matches("^[a-zA-Z0-9 ]++"))

            throw new IllegalArgumentException("INVALID USER NAME!");

    }

    public void validateAadharNumber(String aadharNumber) throws IllegalArgumentException {

        if(!aadharNumber.matches("\\d+"))

            throw new IllegalArgumentException("INVALID AADHAR NUMBER! AADHAR NUMBER MUST CONTAIN ONLY DIGITS!");

        else if(aadharNumber.length() != 12)

            throw new IllegalArgumentException("INVALID AADHAR NUMBER! AADHAR NUMBER MUST CONTAIN 12 DIGITS!");

    }

    public void validateEMailID(String eMailID) throws IllegalArgumentException {

        if(eMailID.split("[@.]").length != 3)

            throw new IllegalArgumentException("INVALID E-MAIL ID!");

    }

    public void validatePassword(String password) throws IllegalArgumentException {

        if(!RailwayUser.Info.validatePassword(password))

            throw new IllegalArgumentException("INSECURE PASSWORD!\n" +
                                       "\nNOTE: THE PASSWORD MUST CONTAIN: " +
                                       "\n1. AT LEAST EIGHT CHARACTERS AND AT MOST 20 CHARACTERS WITHOUT SPACE." +
                                       "\n2. AT LEAST ONE DIGIT." +
                                       "\n3. AT LEAST ONE LOWER CASE AND UPPER CASE ALPHABET." +
                                       "\n4. AT LEAST ONE SPECIAL CHARACTER FROM '!@#$%&*()-+=^'.");

        this.password = password;

    }

    public void confirmPassword(String password) throws IllegalArgumentException {

        if(password.compareTo(this.password) != 0)

            throw new IllegalArgumentException("PASSWORD DOESN'T MATCH!");

    }

}