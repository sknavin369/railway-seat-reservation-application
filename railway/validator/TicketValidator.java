package oops.railway.validator;

import oops.railway.object.*;

import java.util.*;

import java.util.regex.*;

import java.security.*;

public final class TicketValidator {

    private TicketValidator() {}

    public static void validatePNRNumberFor(RailwayUser railwayUser, String pnrNumberStringRep) throws IllegalArgumentException, IllegalAccessException {

        if(!pnrNumberStringRep.matches("\\d+"))

            throw new IllegalArgumentException("INVALID PNR NUMBER! PNR NUMBER MUST CONTAIN ONLY DIGITS!");

        else if(railwayUser.getTicketByPNRNumber(Long.parseLong(pnrNumberStringRep)) == null)

            if(railwayUser.getRailwayNetwork().getTicketByPNRNumber(Long.parseLong(pnrNumberStringRep)) != null)

                throw new IllegalAccessException("ACCESS DENIED! THIS PNR NUMBER BELONGS TO ANOTHER USER'S TICKET'S PNR NUMBER!");

            else

                throw new IllegalArgumentException("INVALID PNR NUMBER! ENTER A VALID PNR NUMBER!");

    }

}