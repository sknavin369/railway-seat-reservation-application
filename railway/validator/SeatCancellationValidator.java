package oops.railway.validator;

import oops.railway.object.*;

import java.util.*;

import java.util.regex.*;

import java.security.*;

public final class SeatCancellationValidator {

    private final RailwayTicket railwayTicket;
    
    private final Set<RailwayPassenger> railwayPassengers = new HashSet<RailwayPassenger>();

    public SeatCancellationValidator(RailwayTicket railwayTicket) {

        this.railwayTicket = railwayTicket;

    }

    public void validateAadharNumber(String aadharNumberStringRep) throws IllegalArgumentException, InvalidParameterException {

        RailwayPassenger railwayPassenger = null;

        if(!aadharNumberStringRep.matches("\\d+"))

            throw new IllegalArgumentException("INVALID AADHAR NUMBER! AADHAR NUMBER MUST CONTAIN ONLY DIGITS!");

        else if(aadharNumberStringRep.length() != 12)

            throw new IllegalArgumentException("INVALID AADHAR NUMBER! AADHAR NUMBER MUST CONTAIN 12 DIGITS!");

        else if((railwayPassenger = this.railwayTicket.getPassengerByAadharNumber(Long.parseLong(aadharNumberStringRep))) == null)

            throw new InvalidParameterException("INVALID AADHAR NUMBER! THE AADHAR NUMBER DOESN'T MATCH TO THAT OF THE PASSENGER'S IN THIS TICKET!");

        else if(this.railwayPassengers.contains(railwayPassenger))

            throw new InvalidParameterException("INVALID AADHAR NUMBER! THIS AADHAR HAS BEEN ENTERED ALREADY!");

        this.railwayPassengers.add(railwayPassenger);

    }

    public Set<RailwayPassenger> getPassengers() {

        return this.railwayPassengers;

    }

}