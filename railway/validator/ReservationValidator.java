package oops.railway.validator;

import oops.railway.object.*;

import java.util.*;

import java.util.regex.*;

import java.security.*;

public final class ReservationValidator {

    private final Map<Long, String> passengers = new LinkedHashMap<Long, String>();

    private String lastPassengerName = null;

    /* ******************* */

    public Map<Long, String> getPassengers() {

        return this.passengers;

    }

    public void validatepassengerName(String passengerName) throws IllegalArgumentException {

        if(!passengerName.matches("^[a-zA-Z ]++"))

            throw new IllegalArgumentException("INVALID PASSENGER NAME! ENTER A VALID PASSENGER NAME CONSISTING OF ALPHABETS ONLY!");

        this.lastPassengerName = passengerName;

    }

    public void validateAadharNumber(String aadharNumberStringRep) throws IllegalArgumentException {

        if(!aadharNumberStringRep.matches("\\d+"))

            throw new IllegalArgumentException("INVALID AADHAR NUMBER! AADHAR NUMBER MUST ONLY CONTAIN DIGITS!");

        else if(aadharNumberStringRep.length() != 12)

            throw new IllegalArgumentException("INVALID AADHAR NUMBER! AADHAR NUMBER MUST CONTAIN 12 DIGITS!");

        else if(this.passengers.containsKey(Long.parseLong(aadharNumberStringRep)))

            throw new IllegalArgumentException("INVALID AADHAR NUMBER! THIS AADHAR NUMBER HAS BEEN ENTERED ALREADY!");

        this.passengers.put(Long.parseLong(aadharNumberStringRep), this.lastPassengerName);

        this.lastPassengerName = null;

    }

}