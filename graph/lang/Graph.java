package oops.graph.lang;

import oops.graph.util.*;

import java.io.*;

public interface Graph extends Serializable {

    public static final long serialVersionUID = 1336311759577610L;

    public enum Type {

        DIRECTED, UNDIRECTED

    }

    public static class TerminalPair<T extends Node> implements Serializable {

        private static final long serialVersionUID = 123336311759610L;

        private final T source, destination;

        public TerminalPair(T source, T destination) {

            this.source = source;

            this.destination = destination;

        }

        public T getSource() {

            return this.source;

        }

        public T getDestination() {

            return this.destination;

        }

        @Override
        public String toString() {

            return "From: " + this.getSource().getName() + " | To: " + this.getDestination().getName();

        }

    }

}