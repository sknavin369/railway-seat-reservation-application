package oops.graph.util;

import java.io.*;

public abstract class Branch implements Serializable {

    private static final long serialVersionUID = -1336316291759577610L;

    private final String name;

    public Branch(String name) {

        this.name = name;

    }

    public String getName() {

        return this.name;

    }

    @Override
    public String toString() {

        return this.name;

    }

}