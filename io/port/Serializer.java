package oops.io.port;

import oops.util.*;

import java.io.*;

public final class Serializer<T extends Serializable> implements Serializable, DataTranslator<T> {

    @Override
    public void save(T obj, File file) {

        try {

        	SerializableUtil.serialize(obj, file);

        }

        catch(IOException exception) {

            exception.printStackTrace();

        }

	}

	@Override
	@SuppressWarnings("unchecked")
    public T retrieve(File file) {

        try {

        	return (T) SerializableUtil.deserialize(file);

        }

        catch(IOException | ClassNotFoundException exception) {

            exception.printStackTrace();

        }

        return null;

	}

}