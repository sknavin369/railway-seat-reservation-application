package oops.io.task;

import java.io.*;

import java.util.*;

public final class FileManipulator {

    private FileManipulator() {}

    public static void deleteFilesWithExtensions(String path, String[] sources, String... extensions) {

        for(String source: sources)

            for(File file: new File(path + source).listFiles())

                if(Arrays.stream(extensions).anyMatch(file.getName()::endsWith))

                    file.delete();

   }

   public static void deleteClassFiles(String path, String... sources) {

       FileManipulator.deleteFilesWithExtensions(path, sources, ".class");

   }

   public static boolean renameFile(String pathName, String oldFileName, String newFileName) {

        File oldFile = new File(pathName + oldFileName),
             newFile = new File(pathName + newFileName);

        return oldFile.renameTo(newFile);

   }

}