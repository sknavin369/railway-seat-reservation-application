package oops.util;

import java.util.*;

final class IterableUtil {

    private IterableUtil() {}

    public static<T> Iterator<T> getNextIteratorOf(Iterable<T> iterable, T obj) {

        T value = null;

        Iterator<T> curr = iterable.iterator();

        while(curr.hasNext()) {

            value = curr.next();

            if(value == obj || value.equals(obj))

                return curr.hasNext() ? curr : null;

        }

        return null;

    }

}