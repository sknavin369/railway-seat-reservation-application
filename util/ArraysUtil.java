package oops.util;

import java.util.*;

import java.util.stream.*;

public final class ArraysUtil {

    private ArraysUtil() {}

    public static<T> int indexOf(T[] array, T searchElement) {

        for(int i = 0; i < array.length; i++)

            if(array[i] == searchElement || array[i].equals(searchElement))

                return i;

        return -1;

    }

    @SuppressWarnings("unchecked")
    public static<T> List<T> mergeToList(final T[]... arrays) {

        final List<T> list = new ArrayList<T>();

        for(T[] array: arrays)

            if(array != null)

                for(T object: array)

                    list.add(object);

        return list;

    }

    public static String[] getToStringArray(Object[] array) {

        String[] ans = new String[array.length];

        for(int i = 0; i < array.length; i++)

            ans[i] = String.valueOf(array[i].toString());

        return ans;

    }

}